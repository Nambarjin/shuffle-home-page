import React from "react"

import BurgerMenu from "../../components/BurgerMenu"

import SwiperCore, { Pagination, Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper.scss'
import 'swiper/components/pagination/pagination.scss'
import 'swiper/components/navigation/navigation.scss'

import "./Home.scss"
SwiperCore.use([Pagination, Navigation]);

const Home = () => {
  return (
    <div id="home-page">

      <BurgerMenu />

      <header className="header">
          <div className="wrapper">
            <img src="./assets/logo.png" alt="shuffle-logo" className="header__logo"/>
            <div className="header__web-menu">
                <a href="/" className="header__web-menu__item">Overview</a>
                <a href="/" className="header__web-menu__item">FAQs</a>
                <a href="/" className="header__web-menu__item">Login</a>
                <a href="https://app.elify.com/signup?#!/signup" className="header__web-menu__item btn btn--orange">JOIN SHUFFLE</a>
            </div>
           </div>
      </header>
      
      <section className="section-hero">
          <div className="section-hero__top">
            <img src="./assets/hero-image.png" alt="hero" className="section-hero__image" />
            <div className="section-hero__desc">
              <h1 className="section-hero__desc__title">Simplified sales and marketing for entrepreneurs</h1>
              <p className="section-hero__desc__text">Build your business and drive sales like a pro </p>
              <a href="https://app.elify.com/signup?#!/signup" className="btn btn--green">JOIN SHUFFLE</a>
              <div className="hero-watch">
                <img src="./assets/icon-play.png" alt="icon-play" className="icon-play" />
                <a href="/">Watch Video</a>
              </div>
            </div>
            </div>
            <div className="section-hero__bottom">
              <div className="section-hero__icons">
                <div className="section-hero__icons__item">
                  <img src="./assets/icon-cell.png" alt="icon" className="section-hero__icons__item__image" />
                  <p className="section-hero__icons__item__text">Always have the right materials on hand </p>
                </div>
                <div className="section-hero__icons__item">
                  <img src="./assets/icon-hook.png" alt="icon" className="section-hero__icons__item__image" />
                  <p className="section-hero__icons__item__text">Never let a lead fall through the cracks</p>
                </div>
                <div className="section-hero__icons__item">
                  <img src="./assets/icon-graph.png" alt="icon" className="section-hero__icons__item__image" />
                  <p className="section-hero__icons__item__text">See proof that your strategies work</p>
                </div>
              </div>
          </div>
      </section>
      
      <section className="section-entrepreneurs">
        <div className="wrapper">
          <img src="./assets/call-assistant.png" alt="" className="section-entrepreneurs__image"/>
          <div className="section-entrepreneurs__desc">
            <h1 className="section-entrepreneurs__desc__title">Entrepreneurs like you need to do it all</h1>
            <h2 className="section-entrepreneurs__desc__sub-title"> You’ve got your work cut out for you.</h2>
            <p className="section-entrepreneurs__desc__text">In this business, success doesn't come easy, and your reputation is on the line. To really win, it takes hard work, the right tools...and a little bit of luck.</p>
            <p className="section-entrepreneurs__desc__text">It's natural to feel overwhelmed when you're responsible for finding new leads, making sales, fulfilling orders, and maintaining customer relationships. We think there should be a solution that lets you easily manage the most important parts of your business by providing you with a system for success.</p>
          </div>
        </div>
      </section>

      <section className="section-message">
          <div className="wrapper">
            <img src="./assets/messenger.png" alt="" className="section-message__image"/>
            <div className="section-message__desc">
              <h1 className="section-message__desc__title">Create and share your message. Watch your business grow.</h1>
              <p className="section-message__desc__text">There should be a system designed with your do-it-all needs in mind. Shuffle helps you elevate your brand, build your network and keep track of all your customers. Share your passion and promote your business using real data and a done-for-you system that works.</p>
              <p className="section-message__desc__text">We've helped thousands of business owners like you build something worth sharing.</p>
            </div>
          </div>
      </section>

      <section className="section-build-marketing">
        <div className="wrapper">
              <h1 className="section-build-marketing__title">Don’t let your hard work go to waste</h1>
              <h2 className="section-build-marketing__sub-title">Your time is precious. You need marketing and sales software designed specifically for you.</h2>
              <div className="section-build-marketing__info">
                <img className="section-build-marketing__info__image" alt="" src="./assets/social.png"/>
                <div className="section-build-marketing__info__desc">
                  <h1 className="section-build-marketing__info__desc__title">Build professional marketing collateral</h1>
                  <p className="section-build-marketing__info__desc__text">Build marketing materials that make you look like the professional we know you are. With Shuffle you won’t just make a great first impression, you’ll always have the right marketing material on hand to share with any prospect. </p>
                  <ul className="section-build-marketing__info__desc__list">
                    <li className="section-build-marketing__info__desc__list__item">Build a digital business card.</li>
                    <li className="section-build-marketing__info__desc__list__item">Create mobile landing pages that highlight your brand.</li>
                    <li className="section-build-marketing__info__desc__list__item">Show off your products and services with stunning photos and embedded videos.</li>
                    <li className="section-build-marketing__info__desc__list__item">Share the resources that work with the rest of your team.</li>
                  </ul>
                </div>
              </div>
              <div className="section-build-marketing__info">
                <img className="section-build-marketing__info__image" alt="" src="./assets/grow.png"/>
                <div className="section-build-marketing__info__desc">
                  <h1 className="section-build-marketing__info__desc__title">Build professional marketing collateral</h1>
                  <p className="section-build-marketing__info__desc__text">Build marketing materials that make you look like the professional we know you are. With Shuffle you won’t just make a great first impression, you’ll always have the right marketing material on hand to share with any prospect. </p>
                  <ul className="section-build-marketing__info__desc__list">
                    <li className="section-build-marketing__info__desc__list__item">Build a digital business card.</li>
                    <li className="section-build-marketing__info__desc__list__item">Create mobile landing pages that highlight your brand.</li>
                    <li className="section-build-marketing__info__desc__list__item">Show off your products and services with stunning photos and embedded videos.</li>
                    <li className="section-build-marketing__info__desc__list__item">Share the resources that work with the rest of your team.</li>
                  </ul>
                </div>
              </div>
              <div className="section-build-marketing__info">
                <img className="section-build-marketing__info__image" alt="" src="./assets/dashboard.png"/>
                <div className="section-build-marketing__info__desc">
                  <h1 className="section-build-marketing__info__desc__title">Track & measure your efforts</h1>
                  <p className="section-build-marketing__info__desc__text">It's crucial to learn what's working and what's not. Successful entrepreneurs don't need "magic" to achieve results. They use data to understand what works, then replicate it over and over again to scale their success. With Shuffle you can do this too. </p>
                  <ul className="section-build-marketing__info__desc__list">
                    <li className="section-build-marketing__info__desc__list__item">Track your marketing so you always know how your message is performing.</li>
                    <li className="section-build-marketing__info__desc__list__item">Get real-time notifications when someone views your material.</li>
                    <li className="section-build-marketing__info__desc__list__item">Optimize your results with stats and analytics.</li>
                    <li className="section-build-marketing__info__desc__list__item">Track how much time leads spend on each page and what they’ve clicked on.</li>
                  </ul>
                </div>
              </div>
            </div>
      </section>


      <section className="section-get-started">
        <div className="wrapper">
          <h1 className="section-get-started__title">Getting started with Shuffle is easy!</h1>
          <div className="section-get-started__icons">
            <div className="section-get-started__icon">
              <img src="./assets/plus.png" alt="icon" className="section-get-started__icon__image" />
              <p className="section-get-started__icon__text">Sign up for Shuffle and download the app</p>
            </div>
            <div className="section-get-started__icon">
              <img src="./assets/create.png" alt="icon" className="section-get-started__icon__image" />
              <p className="section-get-started__icon__text">Create your first landing page</p>
            </div>
            <div className="section-get-started__icon">
              <img src="./assets/share.png" alt="icon" className="section-get-started__icon__image" />
              <p className="section-get-started__icon__text">Share it with new contacts and track its success</p>
            </div>
          </div>
          <a href="https://app.elify.com/signup?#!/signup" className="btn btn--orange">JOIN SHUFFLE</a>
        </div>
      </section>

      <section className="section-feedback">
          <h1 className="section-feedback__title">What entrepreneurs love about Shuffle</h1>
            <Swiper
              loop
              slidesPerView={1}
              pagination={{ clickable: true }}
              navigation
            >
              <SwiperSlide>
                <div className="section-feedback__slide">
                  <div className="section-feedback__slide__text">"Many thanks to Shuffle and highly professional and enthusiastic team! They offer a great product and ensure exceptional customer service to help use their product in fullest. They always explain everything in details to meet clients' needs."</div>
                  <div className="section-feedback__slide__info">
                    <img className="section-feedback__slide__image" alt="profile" src="./assets/profile-pic.png" />
                    <div className="section-feedback__slide__info__name">Katherine Stone </div>
                    <div className="section-feedback__slide__info__job">Customer Engagement Lead at ABT Group</div>
                  </div>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="section-feedback__slide">
                    <div className="section-feedback__slide__text">"Many thanks to Shuffle and highly professional and enthusiastic team! They offer a great product and ensure exceptional customer service to help use their product in fullest. They always explain everything in details to meet clients' needs."</div>
                    <div className="section-feedback__slide__info">
                      <img className="section-feedback__slide__image" alt="profile" src="./assets/profile-pic.png" />
                      <div className="section-feedback__slide__info__name">Katherine Stone </div>
                      <div className="section-feedback__slide__info__job">Customer Engagement Lead at ABT Group</div>
                    </div>
                  </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="section-feedback__slide">
                    <div className="section-feedback__slide__text">"Many thanks to Shuffle and highly professional and enthusiastic team! They offer a great product and ensure exceptional customer service to help use their product in fullest. They always explain everything in details to meet clients' needs."</div>
                    <div className="section-feedback__slide__info">
                      <img className="section-feedback__slide__image" alt="profile" src="./assets/profile-pic.png" />
                      <div className="section-feedback__slide__info__name">Katherine Stone </div>
                      <div className="section-feedback__slide__info__job">Customer Engagement Lead at ABT Group</div>
                    </div>
                  </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="section-feedback__slide">
                    <div className="section-feedback__slide__text">"Many thanks to Shuffle and highly professional and enthusiastic team! They offer a great product and ensure exceptional customer service to help use their product in fullest. They always explain everything in details to meet clients' needs."</div>
                    <div className="section-feedback__slide__info">
                      <img className="section-feedback__slide__image" alt="profile" src="./assets/profile-pic.png" />
                      <div className="section-feedback__slide__info__name">Katherine Stone </div>
                      <div className="section-feedback__slide__info__job">Customer Engagement Lead at ABT Group</div>
                    </div>
                  </div>
              </SwiperSlide>
            </Swiper>
      </section>

      <section className="section-bottom">
        <div className="wrapper">
          <img src="./assets/photo-bottom.png" alt="holding-cell" className="section-bottom__image" />
            <div className="section-bottom__info">
              <h1 className="section-bottom__info__title">Simplified sales and marketing for entrepreneurs</h1>
              <p className="section-bottom__info__text">Build your business and drive sales like a pro </p>
              <a href="https://app.elify.com/signup?#!/signup" className="btn btn--green">JOIN SHUFFLE</a>
              <div className="hero-watch">
                <img src="./assets/icon-play.png" alt="icon-play" className="icon-play" />
                <a href="/">Watch Video</a>
              </div>
            </div> 
        </div>
      </section>

      <footer className="footer">
        <div className="wrapper">
          <div className="footer__grid-container">
            <div className="footer__grid-container__logo">
              <img src="./assets/logo-white.png" alt="logo" className="footer__grid-container__logo__image" />
              <p className="footer__grid-container__logo__text">Smart and fast solutions to help you succeed.</p>
            </div>
            <div className="footer__grid-container__product">
              <ul className="footer__list">
                <li className="footer__list__item">Products</li>
                <li className="footer__list__item"><a href="/">Products</a></li>
                <li className="footer__list__item"><a href="/">Features</a></li>
                <li className="footer__list__item"><a href="/">Pricing</a></li>
              </ul>
            </div>
            <div className="footer__grid-container__company">
              <ul className="footer__list">
                <li className="footer__list__item">Company</li>
                <li className="footer__list__item"><a href="/">About Us</a></li>
                <li className="footer__list__item"><a href="/">Contacts</a></li>
              </ul>
            </div>
            <div className="footer__grid-container__support">
              <ul className="footer__list">
                <li className="footer__list__item">Support</li>
                <li className="footer__list__item"><a href="/">Help Center</a></li>
                <li className="footer__list__item"><a href="/">Developers</a></li>
              </ul>
            </div>
            <div className="footer__grid-container__legal">
              <ul className="footer__list">
                <li className="footer__list__item">Legal</li>
                <li className="footer__list__item"><a href="/">Terms & Conditions</a></li>
                <li className="footer__list__item"><a href="/">Privacy Policy</a></li>
              </ul>
              <div className="footer__grid-container__social-icons--desktop">
                <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/fb-icon.png" /></a>
                <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/insta-icon.png" /></a>
                <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/linkedin-icon.png" /></a>
              </div>
            </div>
            <div className="footer__grid-container__social-icons">
              <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/fb-icon.png" /></a>
              <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/insta-icon.png" /></a>
              <a href="/"><img className="footer__grid-container__social-icons__icon" alt="icon" src="./assets/linkedin-icon.png" /></a>
            </div>
            <div className="footer__grid-container__copyrights">Created by <u>Paper Sword B2B</u> All Rights reserved</div>
          </div>
        </div>
      </footer>

    </div>
  )
}

export default Home
